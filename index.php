<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Device Verification || Netflix</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <style>
    /* google fonts start*/
    @import url('https://fonts.googleapis.com/css?family=Kaushan+Script|Saira&display=swap');
    /* google fonts end*/

    body {
      background: #222;
    }

    .form-control {
      border-radius: 0px;
      border: 0px;
    }

    .font-primary {
      font-family: 'Kaushan Script', cursive;
    }

    .font-secondary {
      font-family: 'Saira', sans-serif;
    }

    .form-area {
      border: 1px solid #fff;
      padding: 20px;
      border-radius: 10px;
      margin-top: 20px;
    }

    .table {
      margin-top: 20px;
      width: 100%;
      border-collapse: collapse;
      display: none;
    }

    .table th,
    .table td {
      padding: 12px;
      text-align: left;
      border-bottom: 1px solid #ddd;
      color: #fff;
    }

    .table th {
      background-color: #343a40;
    }

    .table tbody tr:nth-child(odd) {
      background-color: #2c2c2c;
    }

    .table tbody tr:nth-child(even) {
      background-color: #3a3a3a;
    }

    /* Additional styles for mobile */
    @media screen and (max-width: 576px) {
      .form-area {
        padding: 10px;
      }
    }

    /* CSS for the no results text */
    .no-results {
      color: #fff;
      font-size: 18px;
      text-align: center;
      margin-top: 20px;
    }

    .countdown {
      font-size: 18px;
      font-weight: bold;
      text-align: center;

    }
  </style>
</head>

<body>
  <script>
    function isIOS() {
      return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    }

    function addViewportMetaForIOS() {
      if (isIOS()) {
        var metaTag = document.createElement('meta');
        metaTag.setAttribute('name', 'viewport');
        metaTag.setAttribute('content', 'width=device-width, initial-scale=1, maximum-scale=1');
        document.head.appendChild(metaTag);
      }
    }
    addViewportMetaForIOS();
  </script>

  <section id="contact-form">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <!-- project title area start -->
          <div class="title-area text-light">
            <h1 class="font-primary">Device Verification</h1>
          </div>
          <!-- project title area end -->
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2">
          <div class="form-area mt-5 p-lg-4 p-3">
            <div class="alert alert-primary" role="alert">
              If the device shows the notification “Not part of the home…”,
              <ol>
                <li>Select the option <b>"I am Traveling"</b> or <b>"Watch Temporarily".</b></li>
                <li>Then, click the <b>"Send Email"</b> button.</li>
              </ol>
            </div>
            <!-- main form start -->
            <form method="GET">
              <div class="row mb-3">
              </div>
              <dl class="row">
                <dt class="col-sm-2 text-info font-secondary">Email*</dt>
                <dd class="col-sm-10">
                  <input type="email" class="form-control text-info" name="email" placeholder="akunmu@frilsa.com" required>
                </dd>
              </dl>
              <div class="row">
                <div class="col-sm-12">
                  <input type="submit" class="form-control bg-info text-light font-secondary" value="Submit">
                </div>
              </div>
            </form>
            <!-- main form end -->
          </div>
        </div>
      </div>

      <?php if (isset($_GET['email'])) { ?>
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2">
          <div class="form-area mt-5 p-lg-4 p-3">
            <div class="alert alert-primary" role="alert">
              Menampilkan Hasil Dari Email: <?php echo htmlspecialchars($_GET['email']); ?>
                     </div>
<?php
if (isset($_GET['email'])) {
    date_default_timezone_set('Asia/Jakarta');

    include "imap.php";

    $criteria1 = 'SUBJECT "Kode akses sementara Netflix-mu" FROM "Netflix" TO "' . $_GET['email'] . '"';
    $criteria2 = 'SUBJECT "Penting: Cara mengonfirmasi perangkat Netflix-mu." FROM "Netflix" TO "' . $_GET['email'] . '"';
    $criteria3 = 'SUBJECT "Important: How to update your Netflix Household" FROM "Netflix" TO "' . $_GET['email'] . '"';
    $criteria4 = 'SUBJECT "Your Netflix temporary access code" FROM "Netflix" TO "' . $_GET['email'] . '"';
    $criteria5  = 'SUBJECT "Penting: Cara memperbarui Rumah dengan Akun Netflix-mu" FROM "Netflix" TO "' . $_GET['email'] . '"';
    $mailboxes1 = imap_search($connection, $criteria1);
    $mailboxes2 = imap_search($connection, $criteria2);
    $mailboxes3 = imap_search($connection, $criteria3);
    $mailboxes4 = imap_search($connection, $criteria4);
    $mailboxes5 = imap_search($connection, $criteria5);
    
    $mailboxes = []; 

    if (is_array($mailboxes1)) {
        $mailboxes = array_merge($mailboxes, $mailboxes1);
    }
    if (is_array($mailboxes2)) {
        $mailboxes = array_merge($mailboxes, $mailboxes2);
    }
    if (is_array($mailboxes3)) {
        $mailboxes = array_merge($mailboxes, $mailboxes3);
    }
    if (is_array($mailboxes4)) {
        $mailboxes = array_merge($mailboxes, $mailboxes4);
    }

    if (is_array($mailboxes5)) {
        $mailboxes = array_merge($mailboxes, $mailboxes5);
    }

    $email_received_within_range = false;

    if (!empty($mailboxes)) {
        $ten_hours_ago = strtotime('-10 hours');
        arsort($mailboxes);

        foreach ($mailboxes as $mailbox) {
            $header = imap_headerinfo($connection, $mailbox);
            $date = date("Y-m-d H:i:s", $header->udate);

            if (strtotime($date) >= $ten_hours_ago) {
                $email_received_within_range = true; 

                $from = $header->fromaddress;
                $subject = imap_utf8($header->subject);
                $email_id = urlencode(base64_encode($mailbox));
                echo '<table class="table" id="emailTable">
                        <thead>
                            <tr>
                                <th>From</th>
                                <th>Subject</th>
                                <th>Received Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><b>'.$from.'</b></td>
                                <td><b><a href="email_viewer.php?id='.$email_id.'">'.$subject.'</a></b></td>
                                <td><b>'.$date.'</b></td>
                            </tr>
                        </tbody>
                    </table>';
            } 
        }
    }

    if (empty($mailboxes) || !$email_received_within_range) {
        echo '<div class="alert alert-warning alert-dismissible fade show" role="alert" id="noResultsAlert">
                No emails match the search criteria.<hr>
                Pastikan kamu sudah menekan tombol "Kirim Email" di aplikasi netflix 
                <div class="countdown" id="countdown"></div>
              </div>';
    }

    imap_close($connection);
}
?>
          </div>
        </div>
      </div>
      <?php } ?>

    </div>
  </section>

  <section id="footer" class="mt-4">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <p class="text-light">© 2024 <a target="_blank" href="https://akunmu.id">Akunmu Author</a> All Rights Reserved</p>
        </div>
      </div>
    </div>
  </section>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
    crossorigin="anonymous"></script>
  <script>
    // Show the table if there are emails to display
    <?php if (isset($_GET['email']) && !empty($mailboxes) && $email_received_within_range) { ?>
      document.getElementById('emailTable').style.display = 'table';
    <?php } ?>

    // Countdown and auto-refresh
    <?php if (isset($_GET['email']) && (empty($mailboxes) || !$email_received_within_range)) { ?>
      var countdown = 15;
      var countdownElement = document.getElementById('countdown');

      function updateCountdown() {
        countdownElement.innerText = 'Refreshing in ' + countdown + ' seconds...';
        countdown--;
        if (countdown < 0) {
          location.reload();
        } else {
          setTimeout(updateCountdown, 1000);
        }
      }

      updateCountdown();
    <?php } ?>
  </script>
</body>

</html>
