<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Device Verification || Netflix</title>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
         integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
      <style>
         /* google fonts start*/
         @import url('https://fonts.googleapis.com/css?family=Kaushan+Script|Saira&display=swap');
         /* google fonts end*/
         body {
         background: #222;
         }
         .form-control {
         border-radius: 0px;
         border: 0px;
         }
         .font-primary {
         font-family: 'Kaushan Script', cursive;
         }
         .font-secondary {
         font-family: 'Saira', sans-serif;
         }
         .form-area {
         border: 1px solid #fff;
         padding: 20px;
         border-radius: 10px;
         margin-top: 20px;
         }
         .table {
         margin-top: 20px;
         width: 100%;
         border-collapse: collapse;
         display: none;
         }
         .table th,
         .table td {
         padding: 12px;
         text-align: left;
         border-bottom: 1px solid #ddd;
         color: #fff;
         }
         .table th {
         background-color: #343a40;
         }
         .table tbody tr:nth-child(odd) {
         background-color: #2c2c2c;
         }
         .table tbody tr:nth-child(even) {
         background-color: #3a3a3a;
         }
         /* Additional styles for mobile */
         @media screen and (max-width: 576px) {
         .form-area {
         padding: 10px;
         }
         }
         /* CSS for the no results text */
         .no-results {
         color: #fff;
         font-size: 18px;
         text-align: center;
         margin-top: 20px;
         }
      </style>
   </head>
   <body>
      <script>

         function isIOS() {
           return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
         }
         

         function addViewportMetaForIOS() {
           if (isIOS()) {
             var metaTag = document.createElement('meta');
             metaTag.setAttribute('name', 'viewport');
             metaTag.setAttribute('content', 'width=device-width, initial-scale=1, maximum-scale=1');
             document.head.appendChild(metaTag);
           }
         }
         
         addViewportMetaForIOS();
      </script>
      <section id="contact-form">
         <div class="container">
            <div class="row">
               <div class="col-sm-12 text-center">
                  <!-- project title area start -->
                  <div class="title-area text-light">
                     <h1 class="font-primary">Device Verification</h1>
                  </div>
                  <!-- project title area end -->
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2">
                  <div id=":15v" class="ii gt" jslog="20277; u014N:xr6bB; 1:WyIjdGhyZWFkLWY6MTc5ODIwMjYxMTgwNDEzNTUyMSJd; 4:WyIjbXNnLWY6MTc5ODIxNDU5MjUyNDU2MTE2MiJd">
                     <div id=":15u" class="a3s aiL msg8447146202677877574">
                        <div class="adM"></div>
                        <u></u>
                        <div class="m_8447146202677877574body" style="background-color:#e5e5e5;margin-top:0;padding:0;margin:0">
                           <span class="im">
                              <div style="display:none;width:0;height:0;max-height:0;line-height:0;overflow:hidden">
                                 Link ini akan kedaluwarsa dalam 15 menit
                                 <br>
                                 <div style="display:none;width:0;height:0;max-height:0;line-height:0;overflow:hidden">&nbsp;</div>
                              </div>
                           </span>
                           <table width="100%" border="0" class="m_8447146202677877574envelope" cellpadding="0" cellspacing="0" style="background-color:#e5e5e5" bgcolor="#e5e5e5">
                              <tbody>
                                 <tr>
                                    <td align="center" style="background-color:#e5e5e5;margin-top:0" bgcolor="#e5e5e5">
                                       <table align="center" border="0" class="m_8447146202677877574content" cellpadding="0" cellspacing="0" style="background-color:#ffffff;width:500px" width="500" bgcolor="#ffffff">
                                          <tbody>
                                             <tr>
                                                <td>
                                                   <table class="m_8447146202677877574image" width="100%" cellpadding="0" cellspacing="0">
                                                      <tbody>
                                                         <tr>
                                                            <td class="m_8447146202677877574cell m_8447146202677877574content-padding" align="center" style="padding-left:40px;padding-right:40px;padding-top:0">
                                                               <img src="https://ci3.googleusercontent.com/meips/ADKq_NZo8ux5aD3qXJps74XHn12MugoIhoZs1jDorQ41tGhbySV37J-RV40o7h35MdEoqVITqiMt4o175ghhynMgtg2QIEXrFkL4AR3W7-ZwNnVszU7k9G-Wf0n5Xm7zk8f-IYET2yWziNh2gbeKS6_AjW58LMi14pVupF6DPh45F7_ORsuQ5TL-2SKTagf-rYdc9VJlC6sZUk_cgprXqyPR0GLEcu-4yrUeF8lGlsJS0OIKU7bcYWhkOvnVWzPUDztb0VUX1Ra4DRzH-Es83CREj-a4c3_-rVTcMtiK2ScY9ywVvy0GLOUF9UIfjaeWCa6HrwXJ6YX6zdqNYFD4FAnMaTausRbo7U1j62h93HP-xhOlyQ_FIXHp16i9lkBiZDruY_Tv1SA986tB9_gV7_ivS3lgVFD-3SlI5ZKXrQKYGN6a7pN3kZEi0hYZTrNCVHsbWazgujJvNLWoikgnaPVdEl-1Ft7kSFUF1GlWlc8qiK9Bx0r9xl8Nz5kOel3e0Fg2b8PVtxWQD6y2-shJIrEXwGr1quU4z8Jxfe8MHoVHo-tWxg3z3Wb9iB4ncKtlS7F6pGUdQ67kBV6xmzuYMCmJXCCypzCIChcGjcadaEORdMLtav5YIGiZnOSKoHIgAXg6sRndZc_4aCej8GFvxJDvGEeKg1fNhr-eGFnjbBjeJyPEQPRlkKlC7stE=s0-d-e1-ft#https://beaconimages.netflix.net/img/BAQgBEAEa0AJ2THSsgosiINnMMOKPnivWGK4282oQCgPQFvbocQ89F6XoYdQ8V6Rm_bTdAoTzw4VmNjW7szxZW5SIA5LJyrUoypv_uRW4572r1cJQ6yziOjvcWRtQhxJN-kGsC7yY5OrmGkMM--kvslhSuAEelxhZdeErzjuHBvLbBUYSQzMesRDeAqtpXj1jUgq5c9BW4aY1g3lca1iuE0GHcRPh1Ykb8YIZjTqdQ5zPABfPpvsc28ZyZ3nczNShBAeI9xPPZergqRXFQTdVgV9lAx-p4IkUxFCZCAndWDKLsTk93pogQVtcca5Q9u7dB3lQWqb0tpLc9SbXPBfGoU4RpE8Cj48ATBKuadPXneCTJC6jNkygJ-SoxmrqSbBId-VJpOGl9eom7weVeL5VlRBkVGS5T5udZu4gEbnWJFj7OcT_erxqpw3QbbkzNjcJQZMrKr4PbzY." alt="" width="0" border="0" style="border:none;outline:none;border-collapse:collapse;display:block" class="CToWUd" data-bit="iit">
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td align="center">
                                                   <span class="im">
                                                      <a href="https://www.netflix.com/browse?g=60e0dad7-c4b5-4581-9706-7045fe9b73af&amp;lkid=URL_LOGO&amp;lnktrk=EVO" style="color:inherit" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.netflix.com/browse?g%3D60e0dad7-c4b5-4581-9706-7045fe9b73af%26lkid%3DURL_LOGO%26lnktrk%3DEVO&amp;source=gmail&amp;ust=1716009984560000&amp;usg=AOvVaw1UuixJan6OkUDeZtlCxpVd">
                                                         <table class="m_8447146202677877574image" width="100%" cellpadding="0" cellspacing="0">
                                                            <tbody>
                                                               <tr>
                                                                  <td class="m_8447146202677877574cell m_8447146202677877574content-padding" align="left" style="padding-left:40px;padding-right:40px;padding-top:20px">
                                                                     <img src="https://ci3.googleusercontent.com/meips/ADKq_NanW4CgGwRjEPu6W145C0FAUPkNSUfK2Qk70Sk3Zn2ekP6aADG4-gVTyNoqEz-XDsiJ_6ZWMnkWI3bZTOwiLtj2anEZ2dc=s0-d-e1-ft#https://assets.nflxext.com/us/email/gem/nflx.png" alt="Netflix" width="24" border="0" style="border:none;outline:none;border-collapse:collapse;display:block;border-style:none" class="CToWUd" data-bit="iit">
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </a>
                                                      <table align="left" width="100%" cellpadding="0" cellspacing="0">
                                                         <tbody>
                                                            <tr>
                                                               <td align="left" class="m_8447146202677877574content-padding" style="padding-left:40px;padding-right:40px;font-family:'Netflix Sans',Helvetica,Roboto,Segoe UI,sans-serif;font-weight:700;font-size:36px;line-height:43px;letter-spacing:-1px;padding-top:20px;color:#232323">Kamu mengajukan permintaan untuk memperbarui Rumah dengan Akun Netflix-mu?</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                      <table align="left" width="100%" cellpadding="0" cellspacing="0">
                                                         <tbody>
                                                            <tr>
                                                               <td align="left" class="m_8447146202677877574content-padding" style="padding-left:40px;padding-right:40px;font-family:'Netflix Sans',Helvetica,Roboto,Segoe UI,sans-serif;font-weight:400;font-size:16px;line-height:21px;padding-top:20px;color:#232323">Halo, <b><?= ($email_address[1] ?? 'Not found') ?></b></td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </span>
                                                   <table align="left" width="100%" cellpadding="0" cellspacing="0">
                                                      <tbody>
                                                         <tr>
                                                            <td align="left" class="m_8447146202677877574content-padding" style="padding-left:40px;padding-right:40px;font-family:'Netflix Sans',Helvetica,Roboto,Segoe UI,sans-serif;font-weight:400;font-size:16px;line-height:21px;padding-top:20px;color:#232323">Kami menerima permintaan untuk memperbarui Rumah dengan Akun Netflix untuk akunmu pada <?php echo $account_time[1]; ?>.</td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <span class="im">
                                                      <table align="left" width="100%" cellpadding="0" cellspacing="0">
                                                         <tbody>
                                                            <tr>
                                                               <td align="left" class="m_8447146202677877574content-padding" style="padding-left:40px;padding-right:40px;font-family:'Netflix Sans',Helvetica,Roboto,Segoe UI,sans-serif;font-weight:400;font-size:16px;line-height:21px;padding-top:20px;color:#232323">Rumah dengan Akun Netflix adalah kumpulan perangkat Netflix yang terhubung ke jaringan Internetmu.</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                  
                                                      <table width="100%" cellpadding="0" cellspacing="0">
                                                         <tbody>
                                                            <tr>
                                                               <td style="font-size:0;line-height:0;height:20px" height="20">&nbsp;</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </span>
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tbody>
                                                         <tr>
                                                            <td class="m_8447146202677877574content-padding" style="padding-left:40px;padding-right:40px">
                                                               <table class="m_8447146202677877574color-wrapper" width="100%" cellpadding="0" cellspacing="0">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td bgcolor="#FFFFFF" style="border-radius:8px;padding-bottom:0;border:1px solid #eaeaea">
                                                
                                                                           <span class="im">
                                                                              <table class="m_8447146202677877574single-button m_8447146202677877574mobile-100w" align="center" width="100%" cellpadding="0" cellspacing="0">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td class="m_8447146202677877574content-padding" align="center" style="padding-top:20px;padding-left:20px;padding-right:20px">
                                                                                          <table style="background-color:#e50914;border-radius:4px;width:100%" cellpadding="0" cellspacing="0" width="100%" bgcolor="#e50914">
                                                                                             <tbody>
                                                                                                <tr>
                                                                                                   <td align="center" style="font-family:'Netflix Sans',Helvetica,Roboto,Segoe UI,sans-serif;font-weight:700;font-size:14px;line-height:17px;letter-spacing:-0.2px;padding-top:20px;padding:14px 40px;color:#ffffff">
                                                                                                      <a href="<?= ($confirm_device_url[1] ?? 'Not found') ?>" style="font-family:'Netflix Sans',Helvetica,Roboto,Segoe UI,sans-serif;font-weight:700;font-size:14px;line-height:17px;letter-spacing:-0.2px;text-align:center;text-decoration:none;display:block;color:#ffffff" target="_blank">Ya, Itu Saya</a>
                                                                                                   </td>
                                                                                                </tr>
                                                                                             </tbody>
                                                                                          </table>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                              <table align="left" width="100%" cellpadding="0" cellspacing="0">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td align="left" class="m_8447146202677877574content-padding" style="font-family:'Netflix Sans',Helvetica,Roboto,Segoe UI,sans-serif;font-weight:400;font-size:14px;line-height:18px;letter-spacing:-0.25px;padding-top:20px;color:#232323;padding-left:20px;padding-right:20px">* Link akan kedaluwarsa setelah 15 menit.</td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                              <table width="100%" cellpadding="0" cellspacing="0">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="font-size:0;line-height:0;height:20px" height="20">&nbsp;</td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </span>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <span class="im">
                                                      <table align="left" width="100%" cellpadding="0" cellspacing="0">
                                                         <tbody>
                                                            <tr>
                                                               <td align="left" class="m_8447146202677877574content-padding" style="padding-left:40px;padding-right:40px;font-size:14px;line-height:17px;letter-spacing:-0.2px;font-family:'Netflix Sans',Helvetica,Roboto,Segoe UI,sans-serif;font-weight:500;font-family:'Netflix Sans','Helvetica Neue',Roboto,Segoe UI,sans-serif;padding-top:20px;color:#232323">Tim Netflix</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </span>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td align="center" class="m_8447146202677877574footer-shell" style="background-color:#ffffff" bgcolor="#ffffff">
                                                   <table class="m_8447146202677877574footer" width="100%" border="0" cellpadding="0" cellspacing="0">
                                                      <tbody>
                                                         <tr>
                                                            <td align="center" valign="top" class="m_8447146202677877574footer-shell m_8447146202677877574content-padding" style="padding-left:40px;padding-right:40px;background-color:#ffffff" bgcolor="#ffffff">
                                                               <table width="100%" cellpadding="0" cellspacing="0">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td style="font-size:0;line-height:0;height:40px" height="40">&nbsp;</td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                 
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                       
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section id="footer" class="mt-4">
         <div class="container">
            <div class="row">
               <div class="col-sm-12 text-center">
                  <p class="text-light">© 2024 <a target="_blank" href="#">Unknown Author</a> All Rights Reserved</p>
               </div>
            </div>
         </div>
      </section>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
         integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
         crossorigin="anonymous"></script>
   </body>
</html>
