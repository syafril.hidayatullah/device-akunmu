<?php

include "imap.php";
if (isset($_GET['id'])) {
    $email_id = base64_decode(urldecode($_GET['id'])); 
    $header = imap_headerinfo($connection, $email_id);
    $structure = imap_fetchstructure($connection, $email_id);

    $email_content = imap_fetchbody($connection, $email_id, 1);
    $email_content = imap_qprint($email_content); 

    if (strpos(imap_utf8($header->subject), "Kode akses sementara Netflix-mu") !== false) {
        preg_match('/Dapatkan Kode\s*\[([^\]]+)\]/i', $email_content, $confirm_code);
        preg_match('/mengubah sandimu\s*\[([^\]]+)\]/i', $email_content, $change_password_url);
        preg_match('/akunmu pada ([^,]+ , \d+\.\d+ AM GMT-\d+)/i', $email_content, $account_time);
        preg_match('/Pesan ini dikirim oleh Netflix melalui email ke\s*\[([^\]]+)\]/i', $email_content, $email_address);
        include "email_kode_akses.php";
    } elseif (strpos(imap_utf8($header->subject), "Penting: Cara mengonfirmasi perangkat Netflix-mu.") !== false) {
        preg_match('/Ya, Itu Saya\s*\[([^\]]+)\]/i', $email_content, $confirm_device_url);
        preg_match('/mengubah sandimu\s*\[([^\]]+)\]/i', $email_content, $change_password_url);
        preg_match('/akunmu pada ([^,]+ , \d+\.\d+ AM GMT-\d+)/i', $email_content, $account_time);
        preg_match('/Pesan ini dikirim oleh Netflix melalui email ke\s*\[([^\]]+)\]/i', $email_content, $email_address);
        include "email_penting_verif.php";
    } elseif (strpos(imap_utf8($header->subject), "Your Netflix temporary access code") !== false) {
        preg_match('/Get Code\s*\[([^\]]+)\]/i', $email_content, $confirm_code);
        preg_match('/changing your password\s*\[([^\]]+)\]/i', $email_content, $change_password_url);
        preg_match('your account on ([^,]+ , \d+\.\d+ AM GMT-\d+)/i', $email_content, $account_time);
        preg_match('/This message was mailed to\s*\[([^\]]+)\]/i', $email_content, $email_address);
        include "email_code_access.php";
    } elseif (strpos(imap_utf8($header->subject), "Important: How to update your Netflix Household") !== false) {
        preg_match('/Yes, This Was Me\s*\[([^\]]+)\]/i', $email_content, $confirm_device_url);
        preg_match('/changing your password\s*\[([^\]]+)\]/i', $email_content, $change_password_url);
        preg_match('/your account on ([^,]+ , \d+\.\d+ AM GMT-\d+)/i', $email_content, $account_time);
        preg_match('/This message was mailed to\s*\[([^\]]+)\]/i', $email_content, $email_address);
        include "email_important_verify.php";
    }
      elseif (strpos(imap_utf8($header->subject), "Penting: Cara memperbarui Rumah dengan Akun Netflix-mu") !== false) {
        preg_match('/Ya, Itu Saya\s*\[([^\]]+)\]/i', $email_content, $confirm_device_url);
        preg_match('/mengubah sandimu\s*\[([^\]]+)\]/i', $email_content, $change_password_url);
        preg_match('/akunmu pada\s+(.*?)\./', $email_content, $account_time);
        preg_match('/Pesan ini dikirim oleh Netflix melalui email ke\s*\[([^\]]+)\]/i', $email_content, $email_address);
        include "email_penting_verif_rumah.php"; 
    }
      else {
        echo "Subject tidak dikenali.";
    }

    

} else {
    echo "Parameter email ID tidak ditemukan.";
}

imap_close($connection);
?>
